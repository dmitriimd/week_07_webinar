package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

public class BinanceSymbolPriceService implements SymbolPriceService{

    private static final Logger LOGGER = LoggerFactory.getLogger(BinanceSymbolPriceService.class);

    private RestTemplate restTemplate = new RestTemplate();

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName
     * @return
     */
    @Override
    public Symbol getPrice(String symbolName) {
        ResponseEntity<SymbolImpl> response = restTemplate.getForEntity("https://api.binance.com/api/v3/ticker/price?symbol="+symbolName, SymbolImpl.class);
        if(response.getStatusCode() != HttpStatus.OK){
            LOGGER.error("Не получили ответ. Код ответа: {}", response.getStatusCode());
            return null;
        }
        SymbolImpl symbol = response.getBody();
        LOGGER.debug("Получили ответ для {}: {}", symbolName, symbol);
        return symbol;
    }
}
