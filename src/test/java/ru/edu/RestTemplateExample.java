package ru.edu;

import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.SymbolImpl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class RestTemplateExample {

    @Test
    public void example() throws URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();


        RequestEntity<Void> requestEntity = RequestEntity.get("https://api.binance.com/api/v3/ticker/price")
                .header("X-Forwarded-For", "127.0.0.1")
                .header("Cookie", "name=val")
                .accept(MediaType.APPLICATION_JSON)
                .build();


        ResponseEntity<SymbolImpl[]> response = restTemplate.exchange(requestEntity, SymbolImpl[].class);
        assertNotNull(response);

    }


}
