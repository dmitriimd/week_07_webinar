package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

import static org.junit.Assert.assertEquals;

public class ConcurrencyProblems {
    static final Logger LOGGER = LoggerFactory.getLogger(ConcurrencyProblems.class);

    @Test
    public void raceCondition() {

        Thread[] threads = new Thread[2];

        ValueContainer container = new ValueContainer();

        for (int i = 0; i < 2; i++) {
            threads[i] = new Thread(() -> {
                for (int j = 0; j < 10000; j++) {
                    synchronized (container) {
                        container.number = container.number + 1;
                    }
                }
            });
            threads[i].start();
        }

        try {
            threads[1].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        assertEquals(20000, container.number);
    }

    @Test
    public void deadlock() {
        Thread[] threads = new Thread[2];

        ValueContainer[] containers = {new ValueContainer(), new ValueContainer()};


        for (int i = 0; i < 2; i++) {
            final int key = i;
            threads[i] = new Thread(() -> {
                for (int j = 0; j < 10; j++) {
                    LOGGER.info("Захватываем монитор №1 {}", 0);
                    synchronized (containers[0]) {
                        LOGGER.info("Захватываем монитор №2 {}", 1);
                        synchronized (containers[1]) {
                            LOGGER.info("Обработка");
                            containers[key].number++;
                            containers[1 - key].number--;
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            threads[i].start();
        }
        try {
            threads[1].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deadlockReentrantLock() {
        Thread[] threads = new Thread[2];

        ValueContainer[] containers = {new ValueContainer(), new ValueContainer()};


        ReentrantLock[] locks = {new ReentrantLock(), new ReentrantLock()};

        for (int i = 0; i < 2; i++) {
            final int key = i;
            threads[i] = new Thread(() -> {
                int firstLock = key;
                int secondLock = 1 - key;
                for (int j = 0; j < 3; j++) {
                    int trying = 0;
                    while (true) {
                        LOGGER.info("Захватываем монитор №1 {}, попытка {}", firstLock, ++trying);
                        try {
                            if(!locks[firstLock].tryLock(500, TimeUnit.MILLISECONDS)){
                               LOGGER.info("Не захватили монитор №1 {}", firstLock);
                                continue;
                            }
                            Thread.sleep(1000+500*key);
                            try {
                                LOGGER.info("Захватываем монитор №2 {}, попытка {}", secondLock, trying);
                                if(!locks[secondLock].tryLock(500, TimeUnit.MILLISECONDS)){
                                    LOGGER.info("Не захватили монитор №2 {}", secondLock);
                                    continue;
                                }
                                LOGGER.info("Обработка {}", j);
                                Thread.sleep(1000);
                                break;
                            } catch (InterruptedException e) {
                                LOGGER.error("Не удалось получить блокировку №2");
                            } finally {
                                if (locks[secondLock].isHeldByCurrentThread()) {
                                    LOGGER.info("Освобождаем монитор №2 {}", secondLock);
                                    locks[secondLock].unlock();
                                }
                            }

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                            LOGGER.error("Не удалось получить блокировку №1");
                        } finally {
//                            if (locks[firstLock].isHeldByCurrentThread()) {
//                                LOGGER.info("Освобождаем монитор №1 {}", firstLock);
//                                locks[firstLock].unlock();
//                            }
                        }
                    }


                    synchronized (containers[0]) {
                        LOGGER.info("Захватываем монитор №2 {}", 1);
                        synchronized (containers[1]) {
                            LOGGER.info("Обработка");
                            containers[key].number++;
                            containers[1 - key].number--;
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            });
            threads[i].start();
        }
        try {
            threads[1].join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static class ValueContainer {
        int number;
    }

}
