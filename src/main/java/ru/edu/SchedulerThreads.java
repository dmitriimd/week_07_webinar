package ru.edu;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SchedulerThreads {

    public static void main(String[] args) {
        SymbolPriceService service = new BinanceSymbolPriceService();
        SymbolPriceService cached = new CachedSymbolPriceService(service);

        String[] symbols = {"BTCUSDT", "BTCUSDT", "BTCRUB"};

        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(symbols.length);

        for (String symbol : symbols) {
            executorService.scheduleAtFixedRate(new WatcherJob(symbol, cached), 0, 1, TimeUnit.SECONDS);
        }

    }
}
